pragma foreign_keys = ON;
.separator ,

drop table if exists generations;
create table generations (
  generationOrder integer primary key,
  generationName text unique not null
);
.import generations.csv generations

drop table if exists roles;
create table roles (
  roleNo integer primary key,
  roleName text unique not null
);
.import roles.csv roles

drop table if exists people;
create table people (
  personNo integer primary key,
  fullName text unique not null,
  fullNameYomi text not null,
  surnameYomi text not null,
  surname text not null,
  generation text references generations(generationName)
);
.import people.csv people

drop table if exists appearances;
create table appearances (
  recordNo integer primary key,
  personName text references people(fullName),
  appearanceCount integer not null,
  storyID text not null,
  role integer references roles(roleNo)
);
.import appearances.csv appearances

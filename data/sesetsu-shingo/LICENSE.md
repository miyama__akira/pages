<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a>

このディレクトリにある下記のファイルは、[クリエイティブ・コモンズ 表示 4.0 国際 ライセンス](https://creativecommons.org/licenses/by/4.0/)の下に提供されます。

* `rawdata.csv`
  * このCSVファイルは、サイト[「デイリー晋宋春秋」](https://jinsung.chronicle.wiki)の管理人である佐藤氏が同サイト上の[「世説新語索引　素データ」](https://jinsung.chronicle.wiki/d/%C0%A4%C0%E2%BF%B7%B8%EC%BA%F7%B0%FA%A1%A1%C1%C7%A5%C7%A1%BC%A5%BF)において、[クリエイティブ・コモンズ 表示 4.0 国際 ライセンス](https://creativecommons.org/licenses/by/4.0/)の下に公開してくださったものです（正確には、そのページで「素データ」と書かれたカンマ区切りテキストの部分をコピーアンドペーストしてファイルとしたものが、このファイルです）。
  * 今後、本CSVファイル自体に加工を加えた場合や本CSVファイルから別ファイルを生成した場合などは、その旨をここ（`LICENSE.md`）に追記してゆきます。
それらの成果物もまた、[クリエイティブ・コモンズ 表示 4.0 国際 ライセンス](https://creativecommons.org/licenses/by/4.0/)の下に公開する予定です。
  * なお、元データ作成者たる佐藤氏には、Twitterアカウント [`@satohkun_`](https://twitter.com/satohkun_) を通じて連絡をとることが可能です。別名である浪間丿乀斎（なみまへつぽつさい）氏のアカウント [`@HechupochuSai`](https://twitter.com/HechupochuSai) を通じて連絡をとることも可能です。

* `personal-name-dict-generated-from-rawdata.csv`
  * `rawdata.csv` から生成した人名の仮名漢字変換辞書用のファイルです。詳しくは[`README.md`](README.md)をご覧ください。

* `personal-name-dict.txt`
  * これは『世説新語』に出てくる人名の仮名漢字変換辞書用のファイルですが（[詳しい説明は別途記載しました](https://miyama__akira.gitlab.io/pages/sesetsu-shingo/personal-name-dict.html)）、こちらは、本リポジトリの作者であるわたしが作成しました。Twitter等では深山（[`@miyama__akira`](https://twitter.com/miyama__akira)）という名称を使っているので、クレジットにはその名称をお使い下さい。

* `generations.csv`, `roles.csv`, `people.csv`, `appearances.csv`
  * これらは `rawdata.csv` から生成されたデータです。リレーショナルDB的に扱いやすい（と思われる）形にしてあります。 詳しくは[`README.md`](README.md)をご覧ください。

* `Makefile`, `katakana2hiragana.txt`, `create-DB-from-CSV-files.sql`
  * これらは、佐藤氏のデータを加工するためにわたし（深山）が作成したものです。


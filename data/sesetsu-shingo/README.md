# 『世説新語』関連データ置き場

『世説新語』に関して書いた文章・グラフ・表などは[サイトのトップページ](https://miyama__akira.gitlab.io/pages/)からリンクしていますが、ここではデータそのもの（の一部）を公開しています。

## 自作データ
* `personal-name-dict.txt`<br>
『世説新語』の登場人物たちの名前のかな漢字変換用の辞書データ。
詳細は[説明ページ](https://miyama__akira.gitlab.io/pages/sesetsu-shingo/personal-name-dict.html)に書きました。

## 佐藤氏のデータとそこからの派生物
サイト[「デイリー晋宋春秋」](https://jinsung.chronicle.wiki)の管理人である[佐藤氏](https://twitter.com/satohkun_)によりCC BYライセンスの下に提供されたデータを色々加工してみる予定です（ライセンスの詳細は[`LICENSE.md`](LICENSE.md)に書いた通りです）。

* `rawdata.csv`<br>
佐藤氏により[「世説新語索引　素データ」](https://jinsung.chronicle.wiki/d/%C0%A4%C0%E2%BF%B7%B8%EC%BA%F7%B0%FA%A1%A1%C1%C7%A5%C7%A1%BC%A5%BF)において提供されたデータ。

* `personal-name-dict-generated-from-rawdata.csv`<br>
`rawdata.csv` から人名の読みと漢字表記を抽出し、重複を除いて、ひらがなの読みと漢字表記をカンマ区切りで表した辞書データです（五十音順の筈）。
試しに生成してみたものなので、丸括弧・隅付き括弧・スラッシュ等の表記はそのままになっています。また、`personal-name-dict.txt` は3列ですが、こちらは2列です。

* `generations.csv` と `roles.csv` と `people.csv` と `appearances.csv`<br>
`rawdata.csv`に対して多少の正規化を行い、リレーショナルDB的に扱いやすい（と思われる）形に加工したデータです。
`make`を実行すると、これらのCSVファイルが生成され、更にこれらのCSVファイルから`Sesetsu-shingo.db`というデータベースファイルが生成されます（が、これはバイナリファイルなので、`../../.gitignore`により管理対象から外しました）。
なお、これらのCSVファイルも`make`の実行による生成物ではありますが、それにも関わらず管理対象から外していない理由は、`git`や[SQLite](https://sqlite.org/index.html)を使わない人にも簡単にこれらのCSVデータを利用してほしいからです（`git`を使わなくても入手できる形で公開しておくことを優先しました）。

## `rawdata.csv` からの加工用の諸々
* `Makefile`<br>
技術力がないので、とりあえず自分の環境で動く、というレベルのものです。適宜書き直してご利用ください。
* `katakana2hiragana.txt`
* `create-DB-from-CSV-files.sql`<br>
[SQLite](https://sqlite.org/index.html)を利用する前提です。

'use strict';

window.top.onload = () => {
  window.addEventListener('contextmenu', e => {
    const s = window.getSelection().toString();
    if (!s) { return; }
    e.preventDefault();
    const url = 'https://cse.google.com/cse?cx=partner-pub-9009812931490384:9133157758&q=' + s;
    window.open(url, 'dic');
  });
};

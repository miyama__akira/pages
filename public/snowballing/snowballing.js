'use strict';

function addEmojiDecorations(a, b) {
  function setDiv(left) {
    const div = document.createElement('div');
    div.appendChild(document.createTextNode(
      a + ' ' + b + ' ' + a + ' ' + b + '\n' +
      ( left ? b + '\n' + a + '\n' + b + '\n' + a :
               a + '\n' + b + '\n' + a + '\n' + b ) + '\n'
    ));
    div.id = left ? 'bg-left-top' : 'bg-right-top';
    div.setAttribute('aria-hidden', 'true');
    return(div);
  }
  const leftTop = setDiv(true), rightTop = setDiv(false);
  document.body.insertBefore(rightTop, document.body.firstChild);
  document.body.insertBefore(leftTop, rightTop);
}

window.addEventListener('load', () => { addEmojiDecorations('❄️', '☃️'); });

'use strict';

window.addEventListener('load', () => {
  const trs = document.getElementById('records-of-Canopus').getElementsByTagName('tr');
  let evenRow = false;
  for (const tr of trs) {
    if (tr.className != 'latter-era') { evenRow = !evenRow; }
    if (evenRow) { tr.className += ' even-row'; }
  }
});

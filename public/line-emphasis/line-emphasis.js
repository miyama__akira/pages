'use strict';

window.top.addEventListener('load', () => {
  const emFragments = document.querySelectorAll('em.line');
  for (const em of emFragments) {
    const span = document.createElement('span');
    span.className = 'noun-frame ' + em.className;
    em.className = 'line';
    em.parentNode.insertBefore(span, em.nextSibling);
    span.appendChild(em);
  }
});

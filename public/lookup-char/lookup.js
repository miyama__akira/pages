'use strict';

window.addEventListener('load', ()=> {
  document.querySelectorAll('#radicals span.rad').forEach(r => {
    r.addEventListener('click', () => {
      document.charLookup.components.value += r.textContent;
    });
  });
});
window.addEventListener('keypress', e => {
  if (e.keyCode == 13) { e.preventDefault(); lookupInCHISE(); }
});


function lookupInCHISE() {
  const c = document.charLookup.components.value;
  if (c == '') { return; }
  const url = 'http://www.chise.org/ids-find?components=' + encodeURI(c);
  window.open(url, '_chise');
}

function openUnihanAndGlyphWiki() {
  const c = document.charLookup.char.value,
    p = c.codePointAt(0).toString(16),
    unihan = 'https://www.unicode.org/cgi-bin/GetUnihanData.pl?codepoint=' + p,
    glyphwiki = 'http://glyphwiki.org/wiki/u' + p;
  window.open(unihan, '_unihan');
  window.open(glyphwiki, '_glyphwiki');
}


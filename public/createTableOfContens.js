'use strict';

window.addEventListener('load', e => {
  const ToC = document.getElementById('TableOfContens'),
        h2Headers = document.getElementsByTagName('h2');
  let i = 0;
  for (const h2 of h2Headers) {
    h2.id = 'h2_' + (i++);
    ToC.innerHTML += `<a target="_top" href="#${h2.id}">💚${h2.textContent}</a>`;
  }
  const sections = document.getElementsByTagName('section');
  for (const sec of sections) {
    sec.innerHTML += '<nav><a target="_top" href="#TableOfContens">ページ内目次へ</a> | <a target="_top" href="../index.html">全体目次へ</a></nav>';
  }
});

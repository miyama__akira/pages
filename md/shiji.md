# 『史記』へのリンク

検索などの結果、「これが典拠らしい」とわかって、前後をちょっと確認してみたくなった際などに便利かもしれない、と思って、試しにリンクをリスト化してみた。

## 原文（と註）
* [三家註@漢籍電子文獻資料庫](http://hanchi.ihp.sinica.edu.tw/ihp/hanji.htm)
* [三家註@Wikisource](https://zh.wikisource.org/wiki/%E5%8F%B2%E8%A8%98%E4%B8%89%E5%AE%B6%E8%A8%BB)
* [原文@Wikisource](https://zh.wikisource.org/wiki/%E5%8F%B2%E8%A8%98)
* [原文@中國哲學書電子化計劃](https://ctext.org/shiji/zh)

## 訓読、訳註
* 有朋堂書店「漢文叢書」シリーズ……訓読と註。
    * [第1](https://dl.ndl.go.jp/info:ndljp/pid/958958)（補史記 三皇本紀 〜 巻22 漢興以來將相名臣年表第十）
    * [第2](http://dl.ndl.go.jp/info:ndljp/pid/958959)（巻23 禮書第一 〜 巻39 晉世家第九）
    * [第3](https://dl.ndl.go.jp/info:ndljp/pid/1118527)（巻40 楚世家第十 〜 巻60 三王世家第三十）
    * [第4](https://dl.ndl.go.jp/info:ndljp/pid/1118539)（巻61 伯夷列傳第一 〜 巻85 呂不韋列傳第二十五（巻81の途中から巻83の途中までは欠落））
    * [第5](https://dl.ndl.go.jp/info:ndljp/pid/1118551)（巻86 刺客列傳第二十六 〜 巻109 李將軍列傳第四十九）
    * [第6](http://dl.ndl.go.jp/info:ndljp/pid/958961)（巻110 匈奴列傳第五十 〜 巻130 太史公自序第七十）
* 国民文庫刊行会「国訳漢文大成」シリーズ……訓読と註。経子史部 第15巻以外は、公開範囲が「インターネット公開」になっていないようだ（見つけられなかった）。
    * [経子史部 第15巻](https://dl.ndl.go.jp/info:ndljp/pid/926465)（巻61 伯夷列傳第一 〜 巻101 袁盎鼂錯列傳第四十一）
* 玄黄社「和訳漢文叢書」シリーズ……訓読と割註。
    * [和訳史記列伝 上巻](https://dl.ndl.go.jp/info:ndljp/pid/776330)（巻61 伯夷列傳第一 〜 巻101 袁盎鼂錯列傳第四十一）
    * [和訳史記列伝 下巻](https://dl.ndl.go.jp/info:ndljp/pid/776331)（巻102 張釋之馮唐列傳第四十二 〜 巻130 太史公自序第七十）
* 早稲田大学出版部「史記国字解」シリーズ……訓点つきの原文と「講義」と「字義」。「講義」は、訓読文と現代口語文の間くらいの崩し方で、旧仮名遣いにより書かれている。
    * 第1（補史記 三皇本紀 〜 巻12 孝武本紀第十二）
    [@NDL](https://dl.ndl.go.jp/info:ndljp/pid/959952) / 
    [@IA](https://archive.org/details/shikikokujikai01ssumuoft)
    * 第2（巻13 三代世表第一 〜 巻30 平準書第八）
    [@NDL](https://dl.ndl.go.jp/info:ndljp/pid/959953) /
    [@IA](https://archive.org/details/shikikokujikai02ssumuoft)
    * 第3（巻31 吳太伯世家第一 〜 巻42 鄭世家第十二）
    [@NDL](https://dl.ndl.go.jp/info:ndljp/pid/959954) /
    [@IA](https://archive.org/details/shikikokujikai03ssumuoft)
    * 第4（巻43 趙世家第十三 〜 巻60 三王世家第三十）
    [@NDL](https://dl.ndl.go.jp/info:ndljp/pid/959955) /
    [@IA](https://archive.org/details/shikikokujikai04ssumuoft)
    * 第5（巻61 伯夷列傳第一 〜 巻84 屈原賈生列傳第二十四）
    [@NDL](https://dl.ndl.go.jp/info:ndljp/pid/959956) /
    [@IA](https://archive.org/details/shikikokujikai05ssumuoft)
    * 第6（巻85 呂不韋列傳第二十五 〜 巻104 田叔列傳第四十四）
    [@NDL](https://dl.ndl.go.jp/info:ndljp/pid/959957) /
    [@IA](https://archive.org/details/shikikokujikai06ssumuoft)
    * 第7（巻105 扁鵲倉公列傳第四十五 〜 巻117 司馬相如列傳第五十七（上））
    [@NDL](https://dl.ndl.go.jp/info:ndljp/pid/959958) /
    [@IA](https://archive.org/details/shikikokujikai07ssumuoft)
    * 第8（巻117 司馬相如列傳第五十七（下） 〜 巻130 太史公自序第七十）
    [@NDL](https://dl.ndl.go.jp/info:ndljp/pid/959959) /
    [@IA](https://archive.org/details/shikikokujikai08ssumuoft)

# 『尚書』へのリンク

検索などの結果、「これが典拠らしい」とわかって、前後をちょっと確認してみたくなった際などに便利かもしれない、と思って、試しにリンクをリスト化してみた。
リンク先は Wikisource と NDL。
文字自体は（必要に応じて）[中央研究院の漢籍電子文獻資料庫](http://hanchi.ihp.sinica.edu.tw/ihp/hanji.htm)で確認しつつ、句読点の切り方を Wikisource で見ると便利……かもしれない。


## 序
* [序](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E4%B8%80)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/7)

## 虞書
* [堯典](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%A0%AF%E5%85%B8)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E4%BA%8C)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/259)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/11)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/16)
* [舜典](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E8%88%9C%E5%85%B8)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E4%B8%89)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/263)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/22)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/26)
* [大禹謨](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%A4%A7%E7%A6%B9%E8%AC%A8)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%9B%9B#%E5%A4%A7%E7%A6%B9%E8%AC%A8%E7%AC%AC%E4%B8%89)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/268)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/35)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/47)
* [皐陶謨](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E7%9A%90%E9%99%B6%E8%AC%A8)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%9B%9B#%E7%9A%8B%E9%99%B6%E8%AC%A8%E7%AC%AC%E5%9B%9B)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/273)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/43)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/64)
* [益稷](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E7%9B%8A%E7%A8%B7)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E4%BA%94)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/276)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/48)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/73)

## 夏書
* [禹貢](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E7%A6%B9%E8%B2%A2)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%85%AD)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/280)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/56)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/84)
* [甘誓](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E7%94%98%E8%AA%93)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E4%B8%83#%E7%94%98%E8%AA%93%E7%AC%AC%E4%BA%8C)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/286)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/70)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/100)
* [五子之歌](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E4%BA%94%E5%AD%90%E4%B9%8B%E6%AD%8C)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E4%B8%83#%E4%BA%94%E5%AD%90%E4%B9%8B%E6%AD%8C%E7%AC%AC%E4%B8%89)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/286)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/72)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/102)
* [胤征](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E8%83%A4%E5%BE%81)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E4%B8%83#%E8%83%A4%E5%BE%81%E7%AC%AC%E5%9B%9B)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/288)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/74)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/108)

## 商書
* [湯誓](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E6%B9%AF%E8%AA%93)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%85%AB#%E6%B9%AF%E8%AA%93%E7%AC%AC%E4%B8%80)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/290)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/77)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/114)
* [仲虺之誥](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E4%BB%B2%E8%99%BA%E4%B9%8B%E8%AA%A5)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%85%AB#%E4%BB%B2%E8%99%BA%E4%B9%8B%E8%AA%A5%E7%AC%AC%E4%BA%8C)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/291)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/79)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/116)
* [湯誥](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E6%B9%AF%E8%AA%A5)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%85%AB#%E6%B9%AF%E8%AA%A5%E7%AC%AC%E4%B8%89)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/293)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/82)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/123)
* [伊訓](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E4%BC%8A%E8%A8%93)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%85%AB#%E4%BC%8A%E8%A8%93%E7%AC%AC%E5%9B%9B)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/294)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/85)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/126)
* [太甲上](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%A4%AA%E7%94%B2%E4%B8%8A)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%85%AB#%E5%A4%AA%E7%94%B2%E4%B8%8A%E7%AC%AC%E4%BA%94)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/296)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/87)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/133)
* [太甲中](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%A4%AA%E7%94%B2%E4%B8%AD)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%85%AB#%E5%A4%AA%E7%94%B2%E4%B8%AD%E7%AC%AC%E5%85%AD)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/297)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/89)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/138)
* [太甲下](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%A4%AA%E7%94%B2%E4%B8%8B)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%85%AB#%E5%A4%AA%E7%94%B2%E4%B8%8B%E7%AC%AC%E4%B8%83)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/299)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/91)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/140)
* [咸有一德](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%92%B8%E6%9C%89%E4%B8%80%E5%BE%B7)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%85%AB#%E5%92%B8%E6%9C%89%E4%B8%80%E5%BE%B7%E7%AC%AC%E5%85%AB)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/300)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/92)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/143)
* [盤庚上](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E7%9B%A4%E5%BA%9A%E4%B8%8A)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E4%B9%9D#%E7%9B%A4%E5%BA%9A%E4%B8%8A%E7%AC%AC%E4%B9%9D)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/301)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/95)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/147)
* [盤庚中](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E7%9B%A4%E5%BA%9A%E4%B8%AD)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E4%B9%9D#%E7%9B%A4%E5%BA%9A%E4%B8%AD%E7%AC%AC%E5%8D%81)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/305)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/103)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/157)
* [盤庚下](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E7%9B%A4%E5%BA%9A%E4%B8%8B)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E4%B9%9D#%E7%9B%A4%E5%BA%9A%E4%B8%8B%E7%AC%AC%E5%8D%81%E4%B8%80)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/307)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/108)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/163)
* [說命上](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E8%AA%AA%E5%91%BD%E4%B8%8A)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81#%E8%AA%AA%E5%91%BD%E4%B8%8A%E7%AC%AC%E5%8D%81%E4%BA%8C)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/308)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/111)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/165)
* [說命中](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E8%AA%AA%E5%91%BD%E4%B8%AD)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81#%E8%AA%AA%E5%91%BD%E4%B8%AD%E7%AC%AC%E5%8D%81%E4%B8%89)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/310)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/114)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/169)
* [說命下](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E8%AA%AA%E5%91%BD%E4%B8%8B)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81#%E8%AA%AA%E5%91%BD%E4%B8%8B%E7%AC%AC%E5%8D%81%E5%9B%9B)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/311)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/117)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/173)
* [高宗肜日](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E9%AB%98%E5%AE%97%E8%82%9C%E6%97%A5)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81#%E9%AB%98%E5%AE%97%E8%82%9C%E6%97%A5%E7%AC%AC%E5%8D%81%E4%BA%94)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/313)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/121)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/176)
* [西伯戡黎](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E8%A5%BF%E4%BC%AF%E6%88%A1%E9%BB%8E)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81#%E8%A5%BF%E4%BC%AF%E6%88%A1%E9%BB%8E%E7%AC%AC%E5%8D%81%E5%85%AD)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/313)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/122)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/178)
* [微子](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%BE%AE%E5%AD%90)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81#%E5%BE%AE%E5%AD%90%E7%AC%AC%E5%8D%81%E4%B8%83)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/314)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/124)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/180)

## 周書
* [泰誓上](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E6%B3%B0%E8%AA%93%E4%B8%8A)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%B8%80#%E6%B3%B0%E8%AA%93%E4%B8%8A%E7%AC%AC%E4%B8%80)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/316)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/128)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/185)
* [泰誓中](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E6%B3%B0%E8%AA%93%E4%B8%AD)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%B8%80#%E6%B3%B0%E8%AA%93%E4%B8%AD%E7%AC%AC%E4%BA%8C)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/317)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/132)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/190)
* [泰誓下](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E6%B3%B0%E8%AA%93%E4%B8%8B)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%B8%80#%E6%B3%B0%E8%AA%93%E4%B8%8B%E7%AC%AC%E4%B8%89)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/319)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/136)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/194)
* [牧誓](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E7%89%A7%E8%AA%93)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%B8%80#%E7%89%A7%E8%AA%93%E7%AC%AC%E5%9B%9B)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/320)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/139)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/197)
* [武成](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E6%AD%A6%E6%88%90)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%B8%80#%E6%AD%A6%E6%88%90%E7%AC%AC%E4%BA%94)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/322)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/142)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/200)
* [洪範](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E6%B4%AA%E7%AF%84)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%BA%8C)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/325)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/148)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047048/206)
* [旅獒](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E6%97%85%E7%8D%92)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%B8%89#%E6%97%85%E7%8D%92%E7%AC%AC%E4%B8%83)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/331)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/164)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/7)
* [金縢](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E9%87%91%E7%B8%A2)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%B8%89#%E9%87%91%E7%B8%A2%E7%AC%AC%E5%85%AB)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/332)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/168%22%22)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/10)
* [大誥](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%A4%A7%E8%AA%A5)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%B8%89#%E5%A4%A7%E8%AA%A5%E7%AC%AC%E4%B9%9D)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/335)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/172)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/18)
* [微子之命](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%BE%AE%E5%AD%90%E4%B9%8B%E5%91%BD)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%B8%89#%E5%BE%AE%E5%AD%90%E4%B9%8B%E5%91%BD%E7%AC%AC%E5%8D%81)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/338)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/180)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/27)
* [康誥](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%BA%B7%E8%AA%A5)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E5%9B%9B#%E5%BA%B7%E8%AA%A5%E7%AC%AC%E5%8D%81%E4%B8%80)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/339)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/182)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/30)
* [酒誥](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E9%85%92%E8%AA%A5)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E5%9B%9B#%E9%85%92%E8%AA%A5%E7%AC%AC%E5%8D%81%E4%BA%8C)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/344)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/193)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/46)
* [梓材](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E6%A2%93%E6%9D%90)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E5%9B%9B#%E6%A2%93%E6%9D%90%E7%AC%AC%E5%8D%81%E4%B8%89)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/348)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/200)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/55)
* [召誥](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%8F%AC%E8%AA%A5)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%BA%94#%E5%8F%AC%E8%AA%A5%E7%AC%AC%E5%8D%81%E5%9B%9B)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/349)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/203)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/60)
* [洛誥](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E6%B4%9B%E8%AA%A5)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%BA%94#%E6%B4%9B%E8%AA%A5%E7%AC%AC%E5%8D%81%E4%BA%94)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/353)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/211)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/70)
* [多士](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%A4%9A%E5%A3%AB)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E5%85%AD#%E5%A4%9A%E5%A3%AB%E7%AC%AC%E5%8D%81%E5%85%AD)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/357)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/220)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/82)
* [無逸](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E7%84%A1%E9%80%B8)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E5%85%AD#%E7%84%A1%E9%80%B8%E7%AC%AC%E5%8D%81%E4%B8%83)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/360)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/226)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/89)
* [君奭](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%90%9B%E5%A5%AD)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E5%85%AD#%E5%90%9B%E5%A5%AD%E7%AC%AC%E5%8D%81%E5%85%AB)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/363)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/230)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/98)
* [蔡仲之命](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E8%94%A1%E4%BB%B2%E4%B9%8B%E5%91%BD)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%B8%83#%E8%94%A1%E4%BB%B2%E4%B9%8B%E5%91%BD%E7%AC%AC%E5%8D%81%E4%B9%9D)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/367)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/237)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/108)
* [多方](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%A4%9A%E6%96%B9)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%B8%83#%E5%A4%9A%E6%96%B9%E7%AC%AC%E4%BA%8C%E5%8D%81)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/368)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/240)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/111)
* [立政](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E7%AB%8B%E6%94%BF)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%B8%83#%E7%AB%8B%E6%94%BF%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%B8%80)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/372)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/246)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/121)
* [周官](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%91%A8%E5%AE%98)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E5%85%AB#%E5%91%A8%E5%AE%98%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%BA%8C)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/376)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/251)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/130)
* [君陳](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%90%9B%E9%99%B3)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E5%85%AB#%E5%90%9B%E9%99%B3%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%B8%89)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/379)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/256)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/137)
* [顧命](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E9%A1%A7%E5%91%BD)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E5%85%AB#%E9%A1%A7%E5%91%BD%E7%AC%AC%E4%BA%8C%E5%8D%81%E5%9B%9B)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/381)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/259)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/142)
* [康王之誥](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%BA%B7%E7%8E%8B%E4%B9%8B%E8%AA%A5)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%B9%9D#%E5%BA%B7%E7%8E%8B%E4%B9%8B%E8%AA%A5%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%BA%94)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/384)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/264)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/151)
* [畢命](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E7%95%A2%E5%91%BD)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%B9%9D#%E8%A1%B0%E7%95%A2%E5%91%BD%E7%AC%AC%E4%BA%8C%E5%8D%81%E5%85%AD)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/385)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/267)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/154)
* [君牙](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%90%9B%E7%89%99)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%B9%9D#%E5%90%9B%E7%89%99%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%B8%83)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/388)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/270)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/159)
* [冏命](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%86%8F%E5%91%BD)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%B9%9D#%E5%86%8F%E5%91%BD%E7%AC%AC%E4%BA%8C%E5%8D%81%E5%85%AB)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/389)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/272%22%22)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/162)
* [呂刑](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E5%91%82%E5%88%91)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E5%8D%81%E4%B9%9D#%E5%91%82%E5%88%91%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%B9%9D)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/390)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/274)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/165)
* [文侯之命](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E6%96%87%E4%BE%AF%E4%B9%8B%E5%91%BD)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E4%BA%8C%E5%8D%81#%E6%96%87%E4%BE%AF%E4%B9%8B%E5%91%BD%E4%B9%8B%E7%AC%AC%E4%B8%89%E5%8D%81)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/395)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/283)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/179)
* [費誓](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E8%B2%BB%E8%AA%93)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E4%BA%8C%E5%8D%81#%E8%B2%BB%E8%AA%93%E7%AC%AC%E4%B8%89%E5%8D%81%E4%B8%80)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/397)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/286)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/182)
* [秦誓](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8/%E7%A7%A6%E8%AA%93)
    * [尚書正義](https://zh.wikisource.org/wiki/%E5%B0%9A%E6%9B%B8%E6%AD%A3%E7%BE%A9/%E5%8D%B7%E4%BA%8C%E5%8D%81#%E7%A7%A6%E8%AA%93%E7%AC%AC%E4%B8%89%E5%8D%81%E4%BA%8C)
    * [漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/398)
    * [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898779/288)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047050/184)

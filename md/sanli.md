# 三禮へのリンク

検索などの結果、「これが典拠らしい」とわかって、前後をちょっと確認してみたくなった際などに便利かもしれない、と思って、試しにリンクをリスト化してみた。
文字自体は（必要に応じて）[中央研究院の漢籍電子文獻資料庫](http://hanchi.ihp.sinica.edu.tw/ihp/hanji.htm)で確認しつつ、句読点の切り方を Wikisource で見ると便利……かもしれないが、実際に使い道があるかは謎。

リンク先は Wikisource と NDL。
とは言うものの、実際のところ NDL で「インターネット公開」になっている日本語資料があるのは、『禮記』のみかと思われる。
『禮記』は、中身の理解はさておき、挿絵を時々チラ見すると楽しい（小並感）。

Wikisource では本文と註疏とで文字入力に揺れがあり、（コピペしてブラウザ上で検索するときなどに）不便なので、とりあえず現状で揺れのあったものは、双方の表記を挙げてある。
Wikisource 上の文字表記が変わる可能性は常にある訳だけれども……。

## 周禮

* [天官冢宰 第一](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE/%E5%A4%A9%E5%AE%98%E5%86%A2%E5%AE%B0)
    * [註疏・卷一](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%80)
    * [註疏・卷二](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C)……「大宰之職」〜
    * [註疏・卷三](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89)……「小宰之職」〜
    * [註疏・卷四](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%9B%9B)……「膳夫」〜
    * [註疏・卷五](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%94)……「醫師」〜
    * [註疏・卷六](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%85%AD)……「醢人」〜
    * [註疏・卷七](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%83)……「司書」〜
    * [註疏・卷八](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%85%AB)……「世婦」〜
* [地官司徒 第二](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE/%E5%9C%B0%E5%AE%98%E5%8F%B8%E5%BE%92)
    * [註疏・卷九](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B9%9D)
    * [註疏・卷十](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81)……「大司徒之職」〜
    * [註疏・卷十一](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81%E4%B8%80)……「小司徒之職」〜
    * [註疏・卷十二](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81%E4%BA%8C)……「鄉大夫之職」〜
    * [註疏・卷十三](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81%E4%B8%89)……「牧人」〜
    * [註疏・卷十四](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81%E5%9B%9B)……「均人」〜
    * [註疏・卷十五](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81%E4%BA%94)……「質人」〜
    * [註疏・卷十六](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81%E5%85%AD)……「旅師」〜
* [春官宗伯 第三](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE/%E6%98%A5%E5%AE%98%E5%AE%97%E4%BC%AF)
    * [註疏・卷十七](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81%E4%B8%83)
    * [註疏・卷十八](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81%E5%85%AB)……「大宗伯之職」〜
    * [註疏・卷十九](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81%E4%B9%9D)……「小宗伯之職」〜
    * [註疏・卷二十](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81)……「雞人」〜
    * [註疏・卷二十一](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E4%B8%80)……「典命」〜
    * [註疏・卷二十二](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E4%BA%8C)……「塚人」〜
    * [註疏・卷二十三](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E4%B8%89)……「樂師」〜
    * [註疏・卷二十四](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E5%9B%9B)……「磬師」〜
    * [註疏・卷二十五](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E4%BA%94)……「占夢」〜
    * [註疏・卷二十六](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E5%85%AD)……「喪祝」〜
    * [註疏・卷二十七](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E4%B8%83)……「御史」〜
* [夏官司馬 第四](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE/%E5%A4%8F%E5%AE%98%E5%8F%B8%E9%A6%AC)
    * [註疏・卷二十八](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E5%85%AB)
    * [註疏・卷二十九](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E4%B9%9D)……「大司馬之職」〜
    * [註疏・卷三十](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81)……「小司馬之職」〜
    * [註疏・卷三十一](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81%E4%B8%80)……「司士」〜
    * [註疏・卷三十二](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81%E4%BA%8C)……「弁師」〜
    * [註疏・卷三十三](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81%E4%B8%89)……「校人」〜
* [秋官司寇 第五](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE/%E7%A7%8B%E5%AE%98%E5%8F%B8%E5%AF%87)
    * [註疏・卷三十四](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81%E5%9B%9B)
    * [註疏・卷三十五](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81%E4%BA%94)……「小司寇之職」〜
    * [註疏・卷三十六](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81%E5%85%AD)……「司刑」〜
    * [註疏・卷三十七](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81%E4%B8%83)……「條狼氏」〜
    * [註疏・卷三十八](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81%E5%85%AB)……「司儀」〜
* [冬官考工記 第六](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE/%E5%86%AC%E5%AE%98%E8%80%83%E5%B7%A5%E8%A8%98)
    * [註疏・卷三十九](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81%E4%B9%9D)
    * [註疏・卷四十](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%9B%9B%E5%8D%81)……「輈人」〜
    * [註疏・卷四十一](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%9B%9B%E5%8D%81%E4%B8%80)……「玉人之事」〜
    * [註疏・卷四十二](https://zh.wikisource.org/wiki/%E5%91%A8%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%9B%9B%E5%8D%81%E4%BA%8C)……「匠人」〜

## 儀禮
* （序）
    * [註疏・卷一](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%80)
* [士冠禮 第一](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE/%E5%A3%AB%E5%86%A0%E7%A6%AE)
    * [註疏・卷一](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%80)
    * [註疏・卷二](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C)……「陳服於房中西墉下，東領，北上」〜
    * [註疏・卷三](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89)……「若不醴，則醮用酒」〜
* [士昬禮 第二](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE/%E5%A3%AB%E6%98%AC%E7%A6%AE)
    * [註疏・卷四](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%9B%9B)
    * [註疏・卷五](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%94)……「女次，純衣纁袡，立于房中，南面。」（女次，純衣纁衻，立於房中，南面。）〜
    * [註疏・卷六](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%85%AD)……「若舅姑既沒，則婦入三月，乃奠菜。」〜
* [士相見禮 第三](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE/%E5%A3%AB%E7%9B%B8%E8%A6%8B%E7%A6%AE)
    * [註疏・卷七](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%83)
* [鄉飲酒禮 第四](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE/%E9%84%89%E9%A3%B2%E9%85%92%E7%A6%AE)
    * [註疏・卷八](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%85%AB)
    * [註疏・卷九](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B9%9D)……「賔降洗」（賓降洗）〜
    * [註疏・卷十](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81)……「賔北面坐取俎西之觶」(賓北面坐取俎西之觶）〜
* [鄉射禮 第五](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE/%E9%84%89%E5%B0%84%E7%A6%AE)
    * [註疏・卷十一](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81%E4%B8%80)
    * [註疏・卷十二](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81%E4%BA%8C)……「司射猶挾乘矢，以命三耦」〜
    * [註疏・卷十三](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81%E4%B8%89)……「司正降復位」〜
* [燕禮 第六](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE/%E7%87%95%E7%A6%AE)
    * [註疏・卷十四](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81%E5%9B%9B)
    * [註疏・卷十五](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81%E4%BA%94)……「司宮兼卷重席」〜
* [大射 第七](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE/%E5%A4%A7%E5%B0%84)
    * [註疏・卷十六](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81%E5%85%AD)
    * [註疏・卷十七](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81%E4%B8%83)……「賔以虛爵降」（賓以虛爵降）〜
    * [註疏・卷十八](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81%E5%85%AB)……「三耦卒射。賔降，取弓矢于堂西。」（三耦卒射。賓降，取弓矢於堂西。）〜
* [聘禮 第八](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE/%E8%81%98%E7%A6%AE)
    * [註疏・卷十九](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%8D%81%E4%B9%9D)
    * [註疏・卷二十](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81)……「夫人使下大夫勞以二竹簋方」〜
    * [註疏・卷二十一](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E4%B8%80)……「擯者出請。賓裼，奉束帛加璧享。」〜
    * [註疏・卷二十二](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E4%BA%8C)……「饌於東方，亦如之，西北上。」〜
    * [註疏・卷二十三](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E4%B8%89)……「君使卿皮弁，還玉於館。」〜
    * [註疏・卷二十四](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E5%9B%9B)……「賓入竟而死，遂也。」〜
* [公食大夫禮 第九](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE/%E5%85%AC%E9%A3%9F%E5%A4%A7%E5%A4%AB%E7%A6%AE)
    * [註疏・卷二十五](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E4%BA%94)
    * [註疏・卷二十六](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E5%85%AD)……「明日，宾朝服拜赐于朝」（明日，賓朝服拜賜於朝。）〜
* [覲禮 第十](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE/%E8%A6%B2%E7%A6%AE)
    * [註疏・卷二十六下](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E5%85%AD%E4%B8%8B)
    * [註疏・卷二十七](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E4%B8%83)……「四享皆束帛加璧」（四享，皆束帛加璧）〜
* [喪服 第十一](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE/%E5%96%AA%E6%9C%8D)
    * [註疏・卷二十八](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E5%85%AB)
    * [註疏・卷二十九](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%8C%E5%8D%81%E4%B9%9D)……「父爲長子」の「父」〜
    * [註疏・卷三十](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81)……「疏衰裳齐，牡麻絰，冠布纓，削杖，布帶，疏屨三年者，」（疏衰裳齊、牡麻、冠布纓、削杖、布帶、疏屨三年者。）〜
    * [註疏・卷三十一](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81%E4%B8%80)……「繼父同居者」〜
    * [註疏・卷三十二](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81%E4%BA%8C)……「適婦，女子子適人者爲眾昆弟；」の「婦」〜
    * [註疏・卷三十三](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81%E4%B8%89)……「小功布衰裳，牡麻絰，即葛，五月者。」（小功布衰裳、牡麻、即葛五月者。）〜
    * [註疏・卷三十四](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81%E5%9B%9B)……「朋友皆在他邦，袒免，归則已。」（朋友皆在他邦，袒免，歸則已。）〜
* [士喪禮 第十二](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE/%E5%A3%AB%E5%96%AA%E7%A6%AE)
    * [註疏・卷三十五](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81%E4%BA%94)
    * [註疏・卷三十六](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81%E5%85%AD)……「管人汲，不說繘，屈之。」〜
    * [註疏・卷三十七](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81%E4%B8%83)……「厥明，灭燎。」（厥明，滅燎。）〜
* [既夕禮 第十三](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE/%E6%97%A2%E5%A4%95%E7%A6%AE)
    * [註疏・卷三十八](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81%E5%85%AB)
    * [註疏・卷三十九](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%B8%89%E5%8D%81%E4%B9%9D)……「至于圹。陈器于道东西，北上。」（至於廣，陳器於道東西，北上。）〜
    * [註疏・卷四十](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%9B%9B%E5%8D%81)……「至于圹。陈器于道东西，北上。」（至於廣，陳器於道東西，北上。）〜（もしかして卷三十九と重複しているのだろうか……）
    * [註疏・卷四十一](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%9B%9B%E5%8D%81%E4%B8%80)……「设棜于东堂下，南顺，齐于坫。」（設於於東堂下，南順，齊於坫，）〜
* [士虞禮 第十四](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE/%E5%A3%AB%E8%99%9E%E7%A6%AE)
    * [註疏・卷四十二](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%9B%9B%E5%8D%81%E4%BA%8C)
    * [註疏・卷四十三](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%9B%9B%E5%8D%81%E4%B8%89)……「尸謖。祝前，鄉尸；」（屍謖，祝前，鄉屍。）〜
* [特牲饋食禮 第十五](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE/%E7%89%B9%E7%89%B2%E9%A5%8B%E9%A3%9F%E7%A6%AE)
    * [註疏・卷四十四](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%9B%9B%E5%8D%81%E5%9B%9B)
    * [註疏・卷四十五](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%9B%9B%E5%8D%81%E4%BA%94)……「主人及祝升，祝先入，主人従，西面于户内。」（主人及祝升，祝先入，主人從，西面於戶內。）〜
    * [註疏・卷四十六](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%9B%9B%E5%8D%81%E5%85%AD)……「嗣举奠，盥入，北面再拜稽首。」（嗣舉奠，盥，入，北面再拜稽首。）〜
* [少牢饋食禮 第十六](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE/%E5%B0%91%E7%89%A2%E9%A5%8B%E9%A3%9F%E7%A6%AE)
    * [註疏・卷四十七](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%9B%9B%E5%8D%81%E4%B8%83)
    * [註疏・卷四十八](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%9B%9B%E5%8D%81%E5%85%AB)……「卒脀，祝盥于洗，升自西阶。」（卒┵，祝盥於洗，升自西階。）〜
* [有司 第十七](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE/%E6%9C%89%E5%8F%B8)
    * [註疏・卷四十九](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E5%9B%9B%E5%8D%81%E4%B9%9D)
    * [註疏・卷五十](https://zh.wikisource.org/wiki/%E5%84%80%E7%A6%AE%E8%A8%BB%E7%96%8F/%E5%8D%B7%E4%BA%94%E5%8D%81)……「主人降，南面拜众宾于门东，三拜。」（主人降，南面拜眾賓於門東，三拜。）〜


## 禮記
下記リストの「漢籍國字解全書」は、断りのない限り「先哲遺著追補」シリーズの中の禮記の巻へのリンク。
大學と中庸のみは、四書として「先哲遺著」シリーズにも収められているので、そちらへのリンクもつけた。

* ——
    * [正義・序](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/00)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904132/11)（叙說）
* [曲禮上第一](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E6%9B%B2%E7%A6%AE%E4%B8%8A)
    * [正義・卷一・曲禮上第一](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/01)
    * [正義・卷二・曲禮上第一](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/02)
    * [正義・卷三・曲禮上第一](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/03)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904132/65)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047122/40)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/14)
* [曲禮下第二](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E6%9B%B2%E7%A6%AE%E4%B8%8B)
    * [正義・卷四・曲禮下第二](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/04)
    * [正義・卷五・曲禮下第二](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/05)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904132/100)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047122/105)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/29)
* [檀弓上第三](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E6%AA%80%E5%BC%93%E4%B8%8A)
    * [正義・卷六・檀弓上第三](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/06)
    * [正義・卷七・檀弓上第三](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/07)
    * [正義・卷八・檀弓上第三](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/08)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904132/117)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047122/139)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/38)……一部のページが欠けている。
* [檀弓下第四](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E6%AA%80%E5%BC%93%E4%B8%8B)
    * [正義・卷九・檀弓下第四](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/09)
    * [正義・卷十・檀弓下第四](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/10)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904132/150)
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047122/213)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/52)
* [王制第五](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E7%8E%8B%E5%88%B6)
    * [正義・卷十一・王制第五](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/11)
    * [正義・卷十二・王制第五](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/12)
    * [正義・卷十三・王制第五](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/13)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904132/181)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/70)
* [月令第六](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E6%9C%88%E4%BB%A4)
    * [正義・卷十四・月令第六](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/14)
    * [正義・卷十五・月令第六](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/15)
    * [正義・卷十六・月令第六](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/16)
    * [正義・卷十七・月令第六](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/17)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904132/208)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/85)
* [曾子問第七](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E6%9B%BE%E5%AD%90%E5%95%8F)
    * [正義・卷十八・曾子問第七](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/18)
    * [正義・卷十九・曾子問第七](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/19)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904132/242)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/104)
* [文王世子第八](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E6%96%87%E7%8E%8B%E4%B8%96%E5%AD%90)
    * [正義・卷二十・文王世子第八](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/20)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904132/265)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/116)
* [禮運第九](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E7%A6%AE%E9%81%8B)
    * [正義・卷二十一・禮運第九](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/21)
    * [正義・卷二十二・禮運第九](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/22)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904132/278)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/125)
* [禮器第十](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E7%A6%AE%E5%99%A8)
    * [正義・卷二十三・禮器第十](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/23)
    * [正義・卷二十四・禮器第十](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/24)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904132/296)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/135)
* [郊特牲第十一](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E9%83%8A%E7%89%B9%E7%89%B2)
    * [正義・卷二十五・郊特牲第十一](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/25)
    * [正義・卷二十六・郊特牲第十一](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/26)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904132/314)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/144)
* [內則第十二](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E5%85%A7%E5%89%87)
    * [正義・卷二十七・內則第十二](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/27)
    * [正義・卷二十八・內則第十二](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/28)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/8)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/155)
* [玉藻第十三](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E7%8E%89%E8%97%BB)
    * [正義・卷二十九・玉藻第十三](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/29)
    * [正義・卷三十・玉藻第十三](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/30)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/31)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/169)
* [明堂位第十四](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E6%98%8E%E5%A0%82%E4%BD%8D)
    * [正義・卷三十一・明堂位第十四](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/31)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/52)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/180)
* [喪服小記第十五](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E5%96%AA%E6%9C%8D%E5%B0%8F%E8%A8%98)
    * [正義・卷三十二・喪服小記第十五](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/32)
    * [正義・卷三十三・喪服小記第十五](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/33)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/60)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/184)
* [大傳第十六](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E5%A4%A7%E5%82%B3)
    * [正義・卷三十四・大傳第十六](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/34)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/77)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/190)
* [少儀第十七](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E5%B0%91%E5%84%80)
    * [正義・卷三十五・少儀第十七](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/35)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/82)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/194)
* [學記第十八](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E5%AD%B8%E8%A8%98)
    * [正義・卷三十六・學記第十八](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/36)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/95)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/200)
* [樂記第十九](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E6%A8%82%E8%A8%98)
    * [正義・卷三十七・樂記第十九](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/37)
    * [正義・卷三十八・樂記第十九](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/38)
    * [正義・卷三十九・樂記第十九](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/39)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/102)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/205)
* [雜記上第二十](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E9%9B%9C%E8%A8%98%E4%B8%8A)
    * [正義・卷四十・雜記上第二十](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/40)
    * [正義・卷四十一・雜記上第二十](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/41)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/133)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/223)
* [雜記下第二十一](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E9%9B%9C%E8%A8%98%E4%B8%8B)
    * [正義・卷四十二・雜記下第二十一](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/42)
    * [正義・卷四十三・雜記下第二十一](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/43)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/147)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/231)
* [喪大記第二十二](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E5%96%AA%E5%A4%A7%E8%A8%98)
    * [正義・卷四十四・喪大記第二十二](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/44)
    * [正義・卷四十五・喪大記第二十二](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/45)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/165)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/242)
* [祭法第二十三](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E7%A5%AD%E6%B3%95)
    * [正義・卷四十六・祭法第二十三](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/46)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/187)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/253)
* [祭義第二十四](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E7%A5%AD%E7%BE%A9)
    * [正義・卷四十七・祭義第二十四](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/47)
    * [正義・卷四十八・祭義第二十四](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/48)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/192)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/257)
* [祭統第二十五](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E7%A5%AD%E7%B5%B1)
    * [正義・卷四十九・祭統第二十五](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/49)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/210)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/270)
* [經解第二十六](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E7%B6%93%E8%A7%A3)
    * [正義・卷五十・經解第二十六](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/50.1)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/223)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/280)
* [哀公問第二十七](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E5%93%80%E5%85%AC%E5%95%8F)
    * [正義・卷五十・哀公問第二十七](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/50.2)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/226)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/283)
* [仲尼燕居第二十八](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E4%BB%B2%E5%B0%BC%E7%87%95%E5%B1%85)
    * [正義・卷五十・仲尼燕居第二十八](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/50.3)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/232)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/287)
* [孔子閒居第二十九](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E5%AD%94%E5%AD%90%E9%96%92%E5%B1%85)
    * [正義・卷五十一・孔子閒居第二十九](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/51.1)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/238)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/291)
* [坊記第三十](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E5%9D%8A%E8%A8%98)
    * [正義・卷五十一・坊記第三十](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/51.2)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/242)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/293)
* [中庸第三十一](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E4%B8%AD%E5%BA%B8)
    * [正義・卷五十二・中庸第三十一](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/52)
    * [正義・卷五十三・中庸第三十一](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/53)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/253)
    * [漢籍国字解全書（先哲遺著／大学示蒙句解）](https://dl.ndl.go.jp/info:ndljp/pid/898774/84)
    * [漢文叢書（博文館）](https://dl.ndl.go.jp/info:ndljp/pid/980580/139)
    * [漢文叢書（有朋堂／四書）](https://dl.ndl.go.jp/info:ndljp/pid/1150113/30)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/301)……こちらには見出しと注のみ。
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047092/172)
* [表記第三十二](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E8%A1%A8%E8%A8%98)
    * [正義・卷五十四・表記第三十二](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/54)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/275)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/301)
* [緇衣第三十三](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E7%B7%87%E8%A1%A3)
    * [正義・卷五十五・緇衣第三十三](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/55)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/290)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/311)
* [奔喪第三十四](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E5%A5%94%E5%96%AA)
    * [正義・卷五十六・奔喪第三十四](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/56.1)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/300)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/317)
* [問喪第三十五](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E5%95%8F%E5%96%AA)
    * [正義・卷五十六・問喪第三十五](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/56.2)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/307)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/321)
* [服問第三十六](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E6%9C%8D%E5%95%8F)
    * [正義・卷五十七・服問第三十六](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/57.1)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/312)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/324)
* [間傳第三十七](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E9%96%93%E5%82%B3)
    * [正義・卷五十七・間傳第三十七](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/57.2)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/316)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/326)
* [三年問第三十八](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E4%B8%89%E5%B9%B4%E5%95%8F)
    * [正義・卷五十八・三年問第三十八](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/58)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/320)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/329)
* [深衣第三十九](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E6%B7%B1%E8%A1%A3)
    * [正義・卷五十九・深衣第三十九](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/59.1)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/323)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/331)
* [投壺第四十](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E6%8A%95%E5%A3%BA)
    * [正義・卷五十九・投壺第四十](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/59.2)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/326)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/332)
* [儒行第四十一](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E5%84%92%E8%A1%8C)
    * [正義・卷五十九・儒行第四十一](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/59.3)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/330)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/334)
* [大學第四十二](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E5%A4%A7%E5%AD%B8)
    * [正義・卷六十・大學第四十二](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/60)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/336)
    * [漢籍国字解全書（先哲遺著／大學示蒙句解）](https://dl.ndl.go.jp/info:ndljp/pid/898774/52)
    * [漢文叢書（博文館）](https://dl.ndl.go.jp/info:ndljp/pid/980580/37)
    * [漢文叢書（有朋堂／四書）](https://dl.ndl.go.jp/info:ndljp/pid/1150113/18)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/338)……こちらには見出しと注のみ。
    * [経書大講](https://dl.ndl.go.jp/info:ndljp/pid/1047122/5)
* [冠義第四十三](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E5%86%A0%E7%BE%A9)
    * [正義・卷六十一・冠義第四十三](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/61.1)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/347)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/338)
* [昏義第四十四](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E6%98%8F%E7%BE%A9)
    * [正義・卷六十一・昏義第四十四](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/61.2)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/349)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/340)
* [鄉飲酒義第四十五](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E9%84%89%E9%A3%B2%E9%85%92%E7%BE%A9)
    * [正義・卷六十一・鄉飲酒義第四十五](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/61.3)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/353)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/343)
* [射義第四十六](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E5%B0%84%E7%BE%A9)
    * [正義・卷六十二・射義第四十六](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/62.1)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/360)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/347)
* [燕義第四十七](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E7%87%95%E7%BE%A9)
    * [正義・卷六十二・燕義第四十七](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/62.2)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/366)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/351)
* [聘義第四十八](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E8%81%98%E7%BE%A9)
    * [正義・卷六十三・聘義第四十八](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/63.1)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/369)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/353)
* [喪服四制第四十九](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98/%E5%96%AA%E6%9C%8D%E5%9B%9B%E5%88%B6)
    * [正義・卷六十三・喪服四制第四十九](https://zh.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/63.2)
    * [漢籍國字解全書](https://dl.ndl.go.jp/info:ndljp/pid/1904137/373)
    * [漢文叢書（有朋堂）](https://dl.ndl.go.jp/info:ndljp/pid/1118535/356)

## おまけ
* 四庫全書本『三禮圖集注』
    * [Wikisource](https://zh.wikisource.org/wiki/%E4%B8%89%E7%A6%AE%E5%9C%96%E9%9B%86%E6%B3%A8_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC))
    * [Internet Archive](https://archive.org/search.php?query=%E4%B8%89%E7%A6%AE%E5%9C%96%E9%9B%86%E6%B3%A8%20%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8)

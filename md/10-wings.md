# 十翼へのリンク

[六十四卦へのリンク](64-hexagrams.md)は別ページとした。

## 彖傳（上下）
* [有朋堂「漢文叢書」](https://dl.ndl.go.jp/info:ndljp/pid/912088/434)
* 六十四卦それぞれのページに書かれているもの
    * [Wikisource](https://zh.wikisource.org/wiki/%E6%98%93%E5%82%B3#%E3%80%8A%E6%98%93%E7%B6%93%E3%80%8B%E5%85%AD%E5%8D%81%E5%9B%9B%E5%8D%A6%E5%88%97%E8%A1%A8)
    * [Wikisource の『周易正義』](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9)
    * [早稲田大学出版部「漢籍国字解全書」](https://dl.ndl.go.jp/info:ndljp/pid/898776/24)


## 象傳（上下）
* [有朋堂「漢文叢書」](https://dl.ndl.go.jp/info:ndljp/pid/912088/450)
* 六十四卦それぞれのページに書かれているもの
    * [Wikisource](https://zh.wikisource.org/wiki/%E6%98%93%E5%82%B3#%E3%80%8A%E6%98%93%E7%B6%93%E3%80%8B%E5%85%AD%E5%8D%81%E5%9B%9B%E5%8D%A6%E5%88%97%E8%A1%A8)
    * [Wikisource の『周易正義』](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9)
    * [早稲田大学出版部「漢籍国字解全書」](https://dl.ndl.go.jp/info:ndljp/pid/898776/24)


## 繫辭傳（上下）
* Wikisource
	* [上](https://zh.wikisource.org/wiki/%E6%98%93%E5%82%B3/%E7%B9%AB%E8%BE%AD%E4%B8%8A)
	* [下](https://zh.wikisource.org/wiki/%E6%98%93%E5%82%B3/%E7%B9%AB%E8%BE%AD%E4%B8%8B)
* [Wikisource の『周易正義』](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/07.01)
* [有朋堂「漢文叢書」](https://dl.ndl.go.jp/info:ndljp/pid/912088/471)
* [早稲田大学出版部「漢籍国字解全書」](https://dl.ndl.go.jp/info:ndljp/pid/898777/185)
* [平凡社「経書大講」](https://dl.ndl.go.jp/info:ndljp/pid/1047059/159)

## 說卦傳
* [Wikisource](https://zh.wikisource.org/wiki/%E6%98%93%E5%82%B3/%E8%AA%AA%E5%8D%A6)
* [Wikisource の『周易正義』](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/09.01)
* [有朋堂「漢文叢書」](https://dl.ndl.go.jp/info:ndljp/pid/912088/493)
* [早稲田大学出版部「漢籍国字解全書」](https://dl.ndl.go.jp/info:ndljp/pid/898777/253)

## 序卦傳
* [Wikisource](https://zh.wikisource.org/wiki/%E6%98%93%E5%82%B3/%E5%BA%8F%E5%8D%A6)
* [Wikisource の『周易正義』](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/10)
* [有朋堂「漢文叢書」](https://dl.ndl.go.jp/info:ndljp/pid/912088/498)
* [早稲田大学出版部「漢籍国字解全書」](https://dl.ndl.go.jp/info:ndljp/pid/898777/332)

## 雜卦傳
* [Wikisource](https://zh.wikisource.org/wiki/%E6%98%93%E5%82%B3/%E9%9B%9C%E5%8D%A6)
* [Wikisource の『周易正義』](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/11)
* [有朋堂「漢文叢書」](https://dl.ndl.go.jp/info:ndljp/pid/912088/501)
* [早稲田大学出版部「漢籍国字解全書」](https://dl.ndl.go.jp/info:ndljp/pid/898777/343)

## 文言傳
### 乾
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E6%96%87%E8%A8%80#%E4%B9%BE%E6%96%87%E8%A8%80)
* [Wikisource の『周易正義』](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/01%E4%B9%BE)
* [有朋堂「漢文叢書」](https://dl.ndl.go.jp/info:ndljp/pid/912088/489)
* [早稲田大学出版部「漢籍国字解全書」](https://dl.ndl.go.jp/info:ndljp/pid/898777/320)

### 坤
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E6%96%87%E8%A8%80#%E5%9D%A4%E6%96%87%E8%A8%80)
* [Wikisource の『周易正義』](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/01%E5%9D%A4)
* [有朋堂「漢文叢書」](https://dl.ndl.go.jp/info:ndljp/pid/912088/492)
* [早稲田大学出版部「漢籍国字解全書」](https://dl.ndl.go.jp/info:ndljp/pid/898777/329)

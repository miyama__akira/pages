# 『周易』の六十四卦へのリンク

検索などの結果、「これが典拠らしい」とわかって、前後をちょっと確認してみたくなった際などに便利かもしれない、と思って、試しにリンクをリスト化してみた。
リンク先は Wikisource と NDL。
文字自体は（必要に応じて）[中央研究院の漢籍電子文獻資料庫](http://hanchi.ihp.sinica.edu.tw/ihp/hanji.htm)で確認しつつ、句読点の切り方を Wikisource で見ると便利……かもしれない。

また、いきなり「䷂」などと目にしてもこれを文字入力するのも億劫だし、卦の名称もわからないので、ページ内検索をしやすいように、「下から 100010、上から 010001」のようなメモもつけておいた。

[十翼へのリンク](10-wings.md)は別ページとした。

## (1) ䷀ 乾
* 乾下乾上（下から 111111、上から 111111）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E4%B9%BE)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/01%E4%B9%BE)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/401)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/434)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/450)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/24)

## (2) ䷁ 坤
* 坤下坤上（下から 000000、上から 000000）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%9D%A4)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/01%E5%9D%A4)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/402)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/434)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/450)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/36)

## (3) ䷂ 屯
* 震下坎上（下から 100010、上から 010001）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%B1%AF)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/01%E5%B1%AF)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/402)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/435)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/450)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/45)

## (4) ䷃ 蒙
* 坎下艮上（下から 010001、上から 100010）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E8%92%99)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/01%E8%92%99)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/403)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/435)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/451)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/53)

## (5) ䷄ 需
* 乾下坎上（下から 111010、上から 010111）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E9%9C%80)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/02%E9%9C%80)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/403)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/435)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/451)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/59)

## (6) ䷅ 訟
* 坎下乾上（下から 010111、上から 111010）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E8%A8%9F)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/02%E8%A8%9F)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/404)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/436)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/451)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/65)

## (7) ䷆ 師
* 坎下坤上（下から 010000、上から 000010）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%B8%AB)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/02%E5%B8%AB)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/404)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/436)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/452)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/70)

## (8) ䷇ 比
* 坤下坎上（下から 000010、上から 010000）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E6%AF%94)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/02%E6%AF%94)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/405)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/436)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/452)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/76)

## (9) ䷈ 小畜
* 乾下巽上（下から 111011、上から 110111）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%B0%8F%E7%95%9C)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/02%E5%B0%8F%E7%95%9C)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/405)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/436)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/453)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/81)

## (10) ䷉ 履
* 兌下乾上（下から 110111、上から 111011）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%B1%A5)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/02%E5%B1%A5)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/406)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/437)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/453)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/87)

## (11) ䷊ 泰
* 乾下坤上（下から 111000、上から 000111）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E6%B3%B0)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/02%E6%B3%B0)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/407)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/437)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/453)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/93)

## (12) ䷋ 否
* 坤下乾上（下から 000111、上から 111000）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%90%A6)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/02%E5%90%A6)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/407)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/437)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/454)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/99)

## (13) ䷌ 同人
* 離下乾上（下から 101111、上から 111101）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%90%8C%E4%BA%BA)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/02%E5%90%8C%E4%BA%BA)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/408)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/437)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/454)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/104)

## (14) ䷍ 大有
* 乾下離上（下から 111101、上から 101111）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%A4%A7%E6%9C%89)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/02%E5%A4%A7%E6%9C%89)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/408)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/437)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/454)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/110)

## (15) ䷎ 謙
* 艮下坤上（下から 001000、上から 000100）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E8%AC%99)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/02%E8%AC%99)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/409)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/438)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/455)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/115)

## (16) ䷏ 豫
* 坤下震上（下から 000100、上から 001000）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E8%B1%AB)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/02%E8%B1%AB)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/409)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/438)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/455)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/119)

## (17) ䷐ 隨
* 震下兌上（下から 100110、上から 011001）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E9%9A%A8)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/03%E9%9A%A8)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/409)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/438)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/455)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/125)

## (18) ䷑ 蠱
* 巽下艮上（下から 011001、上から 100110）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E8%A0%B1)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/03%E8%A0%B1)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/410)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/438)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/456)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/130)

## (19) ䷒ 臨
* 兌下坤上（下から 110000、上から 000011）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E8%87%A8)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/03%E8%87%A8)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/411)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/439)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/456)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/135)

## (20) ䷓ 觀
* 坤下巽上（下から 000011、上から 110000）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E8%A7%80)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/03%E8%A7%80)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/411)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/439)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/456)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/139)

## (21) ䷔ 噬嗑
* 震下離上（下から 100101、上から 101001）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%99%AC%E5%97%91)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/03%E5%99%AC%E5%97%91)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/411)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/439)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/457)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/144)

## (22) ䷕ 賁
* 離下艮上（下から 101001、上から 100101）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E8%B3%81)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/03%E8%B3%81)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/412)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/439)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/457)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/149)

## (23) ䷖ 剝
* 坤下艮上（下から 000001、上から 100000）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%89%9D)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/03%E5%89%9D)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/412)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/439)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/457)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/153)

## (24) ䷗ 復
* 震下坤上（下から 100000、上から 000001）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%BE%A9)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/03%E5%BE%A9)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/413)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/440)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/458)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/157)

## (25) ䷘ 无妄
* 震下乾上（下から 100111、上から 111001）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E6%97%A0%E5%A6%84)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/03%E6%97%A0%E5%A6%84)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/413)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/440)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/458)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/163)

## (26) ䷙ 大畜
* 乾下艮上（下から 111001、上から 100111）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%A4%A7%E7%95%9C)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/03%E5%A4%A7%E7%95%9C)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/414)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/440)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/458)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/168)

## (27) ䷚ 頤
* 震下艮上（下から 100001、上から 100001）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E9%A0%A4)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/03%E9%A0%A4)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/414)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/440)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/459)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/173)

## (28) ䷛ 大過
* 巽下兌上（下から 011110、上から 011110）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%A4%A7%E9%81%8E)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/03%E5%A4%A7%E9%81%8E)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/415)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/440)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/459)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/179)

## (29) ䷜ 坎
* 坎下坎上（下から 010010、上から 010010）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%9D%8E)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/03%E5%9D%8E)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/415)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/441)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/459)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/184)

## (30) ䷝ 離
* 離下離上（下から 101101、上から 101101）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E9%9B%A2)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/03%E9%9B%A2)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/416)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/441)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/460)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898776/190)

## (31) ䷞ 咸
* 艮下兌上（下から 001110、上から 011100）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%92%B8)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/04%E5%92%B8)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/417)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/442)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/460)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/6)

## (32) ䷟ 恒
* 巽下震上（下から 011100、上から 001110）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E6%81%92)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/04%E6%81%92)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/417)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/442)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/460)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/12)

## (33) ䷠ 遯
* 艮下乾上（下から 001111、上から 111100）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E9%81%AF)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/04%E9%81%AF)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/418)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/442)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/461)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/18)

## (34) ䷡ 大壯
* 乾下震上（下から 111100、上から 001111）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%A4%A7%E5%A3%AF)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/04%E5%A4%A7%E5%A3%AF)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/418)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/442)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/461)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/22)

## (35) ䷢ 晉
* 坤下離上（下から 000101、上から 101000）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E6%99%89)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/04%E6%99%89)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/418)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/442)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/461)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/26)

## (36) ䷣ 明夷
* 離下坤上（下から 101000、上から 000101）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E6%98%8E%E5%A4%B7)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/04%E6%98%8E%E5%A4%B7)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/419)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/443)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/462)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/31)

## (37) ䷤ 家人
* 離下巽上（下から 101011、上から 110101）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%AE%B6%E4%BA%BA)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/04%E5%AE%B6%E4%BA%BA)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/420)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/443)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/462)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/36)

## (38) ䷥ 睽
* 兌下離上（下から 110101、上から 101011）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E7%9D%BD)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/04%E7%9D%BD)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/420)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/443)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/462)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/41)

## (39) ䷦ 蹇
* 艮下坎上（下から 001010、上から 010100）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E8%B9%87)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/04%E8%B9%87)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/421)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/443)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/463)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/46)

## (40) ䷧ 解
* 坎下震上（下から 010100、上から 001010）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E8%A7%A3)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/04%E8%A7%A3)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/421)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/440)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/463)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/50)

## (41) ䷨ 損
* 兌下艮上（下から 110001、上から 100011）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E6%90%8D)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/04%E6%90%8D)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/421)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/440)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/463)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/56)

## (42) ䷩ 益
* 震下巽上（下から 100011、上から 110001）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E7%9B%8A)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/04%E7%9B%8A)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/422)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/440)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/464)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/61)

## (43) ䷪ 夬
* 乾下兌上（下から 111110、上から 011111）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%A4%AC)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/05%E5%A4%AC)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/423)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/445)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/464)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/67)

## (44) ䷫ 姤
* 巽下乾上（下から 011111、上から 111110）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%A7%A4)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/05%E5%A7%A4)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/423)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/445)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/464)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/72)

## (45) ䷬ 萃
* 坤下兌上（下から 000110、上から 011000）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E8%90%83)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/05%E8%90%83)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/424)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/445)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/465)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/77)

## (46) ䷭ 升
* 巽下坤上（下から 011000、上から 000110）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%8D%87)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/05%E5%8D%87)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/424)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/445)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/465)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/83)

## (47) ䷮ 困
* 坎下兌上（下から 010110、上から 011010）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%9B%B0)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/05%E5%9B%B0)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/425)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/446)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/465)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/87)

## (48) ䷯ 井
* 巽下坎上（下から 011010、上から 010110）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E4%BA%95)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/05%E4%BA%95)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/425)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/446)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/465)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/94)

## (49) ䷰ 革
* 離下兌上（下から 101110、上から 011101）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E9%9D%A9)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/05%E9%9D%A9)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/426)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/446)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/466)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/99)

## (50) ䷱ 鼎
* 巽下離上（下から 011101、上から 101110）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E9%BC%8E)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/05%E9%BC%8E)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/426)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/446)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/466)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/104)

## (51) ䷲ 震
* 震下震上（下から 100100、上から 001001）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E9%9C%87)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/05%E9%9C%87)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/427)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/446)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/467)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/110)

## (52) ䷳ 艮
* 艮下艮上（下から 001001、上から 100100）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E8%89%AE)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/05%E8%89%AE)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/427)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/447)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/467)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/116)

## (53) ䷴ 漸
* 艮下巽上（下から 001011、上から 110100）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E6%BC%B8)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/05%E6%BC%B8)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/428)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/447)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/467)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/121)

## (54) ䷵ 歸妹
* 兌下震上（下から 110100、上から 001011）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E6%AD%B8%E5%A6%B9)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/05%E6%AD%B8%E5%A6%B9)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/429)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/447)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/468)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/126)

## (55) ䷶ 豐
* 離下震上（下から 101100、上から 001101）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E8%B1%90)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/06%E8%B1%90)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/429)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/447)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/468)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/132)

## (56) ䷷ 旅
* 艮下離上（下から 001101、上から 101100）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E6%97%85)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/06%E6%97%85)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/430)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/448)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/468)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/137)

## (57) ䷸ 巽
* 巽下巽上（下から 011011、上から 110110）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%B7%BD)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/06%E5%B7%BD)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/430)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/448)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/469)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/143)

## (58) ䷹ 兌
* 兌下兌上（下から 110110、上から 011011）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%85%8C)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/06%E5%85%8C)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/431)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/448)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/469)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/148)

## (59) ䷺ 渙
* 坎下巽上（下から 010011、上から 110010）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E6%B8%99)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/06%E6%B8%99)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/431)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/448)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/469)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/152)

## (60) ䷻ 節
* 兌下坎上（下から 110010、上から 010011）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E7%AF%80)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/06%E7%AF%80)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/432)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/448)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/470)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/158)

## (61) ䷼ 中孚
* 兌下巽上（下から 110011、上から 110011）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E4%B8%AD%E5%AD%9A)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/06%E4%B8%AD%E5%AD%9A)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/432)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/449)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/470)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/163)

## (62) ䷽ 小過
* 艮下震上（下から 001100、上から 001100）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E5%B0%8F%E9%81%8E)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/06%E5%B0%8F%E9%81%8E)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/432)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/449)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/470)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/170)

## (63) ䷾ 既濟
* 離下坎上（下から 101010、上から 010101）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E6%97%A2%E6%BF%9F)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/06%E6%97%A2%E6%BF%9F)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/433)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/449)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/470)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/175)

## (64) ䷿ 未濟
* 坎下離上（下から 010101、上から 101010）
* [Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93/%E6%9C%AA%E6%BF%9F)
* [周易正義@Wikisource](https://zh.wikisource.org/wiki/%E5%91%A8%E6%98%93%E6%AD%A3%E7%BE%A9/06%E6%9C%AA%E6%BF%9F)
* [經@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/433)
* [彖@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/449)
* [象@漢文叢書](https://dl.ndl.go.jp/info:ndljp/pid/912088/471)
* [漢籍国字解全書](https://dl.ndl.go.jp/info:ndljp/pid/898777/180)


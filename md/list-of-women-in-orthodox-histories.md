# 正史で女子会（？）

Wikisource の正史の目次をぱっと見て、女性が載っているのが明らかな巻へのリンク集を作ってみた。
とりあえず『清史稿』も対象にした。

* 『史記』
    * [卷九　呂后本紀](https://zh.wikisource.org/wiki/%E5%8F%B2%E8%A8%98/%E5%8D%B7009)
* 『漢書』
    * [卷三　高后紀](https://zh.wikisource.org/wiki/%E6%BC%A2%E6%9B%B8/%E5%8D%B7003)
    * [卷九十七上　外戚傳](https://zh.wikisource.org/wiki/%E6%BC%A2%E6%9B%B8/%E5%8D%B7097%E4%B8%8A)
    * [卷九十七下　外戚傳](https://zh.wikisource.org/wiki/%E6%BC%A2%E6%9B%B8/%E5%8D%B7097%E4%B8%8B)
    * [卷九十八　元后傳](https://zh.wikisource.org/wiki/%E6%BC%A2%E6%9B%B8/%E5%8D%B7098)
* 『後漢書』
    * [卷十上　皇后紀](https://zh.wikisource.org/wiki/%E5%BE%8C%E6%BC%A2%E6%9B%B8/%E5%8D%B710%E4%B8%8A)
    * [卷十下　皇后紀](https://zh.wikisource.org/wiki/%E5%BE%8C%E6%BC%A2%E6%9B%B8/%E5%8D%B710%E4%B8%8B)
    * [卷八十四　列女傳](https://zh.wikisource.org/wiki/%E5%BE%8C%E6%BC%A2%E6%9B%B8/%E5%8D%B784)
* 『三國志』
    * [卷五　魏書五　后妃傳](https://zh.wikisource.org/wiki/%E4%B8%89%E5%9C%8B%E5%BF%97/%E5%8D%B705)
    * [卷十八　魏書十八　趙娥傳](https://zh.wikisource.org/wiki/%E4%B8%89%E5%9C%8B%E5%BF%97/%E5%8D%B718#%E6%AF%8D_%E8%B6%99%E5%A8%A5)
    * [卷三十四　蜀書四　二主妃子傳](https://zh.wikisource.org/wiki/%E4%B8%89%E5%9C%8B%E5%BF%97/%E5%8D%B734#%E7%9A%87%E5%90%8E)
    * [卷五十　吳書五　妃嬪傳](https://zh.wikisource.org/wiki/%E4%B8%89%E5%9C%8B%E5%BF%97/%E5%8D%B750)
* 『晉書』
    * [卷三十一　后妃傳上](https://zh.wikisource.org/wiki/%E6%99%89%E6%9B%B8/%E5%8D%B7031)
    * [卷三十二　后妃傳下](https://zh.wikisource.org/wiki/%E6%99%89%E6%9B%B8/%E5%8D%B7032)
    * [卷四十　郭槐傳](https://zh.wikisource.org/wiki/%E6%99%89%E6%9B%B8/%E5%8D%B7040#%E5%85%85%E5%A9%A6_%E9%83%AD%E6%A7%90)
    * [卷九十六　列女傳](https://zh.wikisource.org/wiki/%E6%99%89%E6%9B%B8/%E5%8D%B7096)
* 『宋書』
    * [卷四十一　后妃傳](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B741)
* 『南齊書』
    * [卷二十　皇后傳](https://zh.wikisource.org/wiki/%E5%8D%97%E9%BD%8A%E6%9B%B8/%E5%8D%B720)
* 『梁書』
    * [卷七　皇后傳](https://zh.wikisource.org/wiki/%E6%A2%81%E6%9B%B8/%E5%8D%B707)
* 『陳書』
    * [卷七　皇后傳](https://zh.wikisource.org/wiki/%E9%99%B3%E6%9B%B8/%E5%8D%B77)
* 『魏書』
    * [卷十三　皇后傳](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B713)
    * [卷九十二　列女傳](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B792)
* 『北齊書』
    * [卷九　後宮傳](https://zh.wikisource.org/wiki/%E5%8C%97%E9%BD%8A%E6%9B%B8/%E5%8D%B79)
* 『周書』
    * [卷九　皇后傳](https://zh.wikisource.org/wiki/%E5%91%A8%E6%9B%B8/%E5%8D%B709)
* 『隋書』
    * [卷三十六　后妃傳](https://zh.wikisource.org/wiki/%E9%9A%8B%E6%9B%B8/%E5%8D%B736)
    * [卷八十　列女傳](https://zh.wikisource.org/wiki/%E9%9A%8B%E6%9B%B8/%E5%8D%B780)
* 『南史』
    * [卷十一　后妃傳上](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B711)
    * [卷十二　后妃傳下](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B712)
    * [卷七十三　孝義傳上](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B773)
    * [卷七十四　孝義傳下](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B774)
* 『北史』
    * [卷十三　后妃傳上](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7013)
    * [卷十四　后妃傳下](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7014)
    * [卷九十一　列女傳](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7091)
* 『舊唐書』
    * [卷六　則天皇后本紀](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B76)
    * [卷五十一　后妃傳上](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B751)
    * [卷五十二　后妃傳下](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B752)
    * [卷一百九十三　列女傳](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B7193)
* 『新唐書』
    * [卷四　則天皇后本紀](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7004#%E5%89%87%E5%A4%A9%E7%9A%87%E5%90%8E)
    * [卷七十六　后妃傳上](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7076)
    * [卷七十七　后妃傳下](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7077)
    * [卷八十三　諸帝公主傳](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7083)
    * [卷二百零五　列女傳](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7205)
* 『舊五代史』
    * [卷十一　梁書十一　后妃列傳](https://zh.wikisource.org/wiki/%E8%88%8A%E4%BA%94%E4%BB%A3%E5%8F%B2/%E5%8D%B711)
    * [卷四十九　唐書二十五　后妃列傳](https://zh.wikisource.org/wiki/%E8%88%8A%E4%BA%94%E4%BB%A3%E5%8F%B2/%E5%8D%B749)
    * [卷八十六　晉書十二　后妃列傳](https://zh.wikisource.org/wiki/%E8%88%8A%E4%BA%94%E4%BB%A3%E5%8F%B2/%E5%8D%B786)
    * [卷一百〇四　漢書六　后妃列傳](https://zh.wikisource.org/wiki/%E8%88%8A%E4%BA%94%E4%BB%A3%E5%8F%B2/%E5%8D%B7104)
    * [卷一百二十一　周書十二　后妃列傳](https://zh.wikisource.org/wiki/%E8%88%8A%E4%BA%94%E4%BB%A3%E5%8F%B2/%E5%8D%B7121)
* 『新五代史』
    * [卷十三　梁家人傳](https://zh.wikisource.org/wiki/%E6%96%B0%E4%BA%94%E4%BB%A3%E5%8F%B2/%E5%8D%B713)
    * [卷十四　唐太祖家人傳](https://zh.wikisource.org/wiki/%E6%96%B0%E4%BA%94%E4%BB%A3%E5%8F%B2/%E5%8D%B714)
    * [卷十五　唐明宗家人傳](https://zh.wikisource.org/wiki/%E6%96%B0%E4%BA%94%E4%BB%A3%E5%8F%B2/%E5%8D%B715)
    * [卷十六　唐廢帝家人傳](https://zh.wikisource.org/wiki/%E6%96%B0%E4%BA%94%E4%BB%A3%E5%8F%B2/%E5%8D%B716)
    * [卷十七　晉家人傳](https://zh.wikisource.org/wiki/%E6%96%B0%E4%BA%94%E4%BB%A3%E5%8F%B2/%E5%8D%B717)
    * [卷十八　漢家人傳](https://zh.wikisource.org/wiki/%E6%96%B0%E4%BA%94%E4%BB%A3%E5%8F%B2/%E5%8D%B718)
    * [卷十九　周太祖家人傳](https://zh.wikisource.org/wiki/%E6%96%B0%E4%BA%94%E4%BB%A3%E5%8F%B2/%E5%8D%B719)
    * [卷二十　周世宗家人傳](https://zh.wikisource.org/wiki/%E6%96%B0%E4%BA%94%E4%BB%A3%E5%8F%B2/%E5%8D%B720)
* 『宋史』
    * [巻二百四十二　后妃傳上](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7242)
    * [巻二百四十三　后妃傳下](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7243)
    * [巻二百四十八　公主傳](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7248)
    * [巻四百六十　列女傳](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7460)
* 『遼史』
    * [卷六十五　公主表](https://zh.wikisource.org/wiki/%E9%81%BC%E5%8F%B2/%E5%8D%B765)
    * [卷七十一　后妃傳](https://zh.wikisource.org/wiki/%E9%81%BC%E5%8F%B2/%E5%8D%B771)
    * [卷一百〇七　列女傳](https://zh.wikisource.org/wiki/%E9%81%BC%E5%8F%B2/%E5%8D%B7107)
* 『金史』
    * [卷六十三　后妃傳上](https://zh.wikisource.org/wiki/%E9%87%91%E5%8F%B2/%E5%8D%B763)
    * [卷六十四　后妃傳下](https://zh.wikisource.org/wiki/%E9%87%91%E5%8F%B2/%E5%8D%B764)
    * [卷一百三十　列女傳](https://zh.wikisource.org/wiki/%E9%87%91%E5%8F%B2/%E5%8D%B7130)
* 『元史』
    * [卷一百〇六　后妃表](https://zh.wikisource.org/wiki/%E5%85%83%E5%8F%B2/%E5%8D%B7106)
    * [卷一百〇九　諸公主表](https://zh.wikisource.org/wiki/%E5%85%83%E5%8F%B2/%E5%8D%B7109)
    * [卷一百十四　后妃傳一](https://zh.wikisource.org/wiki/%E5%85%83%E5%8F%B2/%E5%8D%B7114)
    * [卷一百十六　后妃傳二](https://zh.wikisource.org/wiki/%E5%85%83%E5%8F%B2/%E5%8D%B7116)
    * [卷二百　列女傳一](https://zh.wikisource.org/wiki/%E5%85%83%E5%8F%B2/%E5%8D%B7200)
    * [卷二百〇一　列女傳二](https://zh.wikisource.org/wiki/%E5%85%83%E5%8F%B2/%E5%8D%B7201)
* 『明史』
    * [卷一百十三　后妃傳一](https://zh.wikisource.org/wiki/%E6%98%8E%E5%8F%B2/%E5%8D%B7113)
    * [卷一百十四　后妃傳二](https://zh.wikisource.org/wiki/%E6%98%8E%E5%8F%B2/%E5%8D%B7114)
    * [卷一百二十一　公主傳](https://zh.wikisource.org/wiki/%E6%98%8E%E5%8F%B2/%E5%8D%B7121)
    * [卷二百七十　秦良玉傳](https://zh.wikisource.org/wiki/%E6%98%8E%E5%8F%B2/%E5%8D%B7270#%E7%A7%A6%E8%89%AF%E7%8E%89)
    * [卷三百〇一　列女傳一](https://zh.wikisource.org/wiki/%E6%98%8E%E5%8F%B2/%E5%8D%B7301)
    * [卷三百〇二　列女傳二](https://zh.wikisource.org/wiki/%E6%98%8E%E5%8F%B2/%E5%8D%B7302)
    * [卷三百〇三　列女傳三](https://zh.wikisource.org/wiki/%E6%98%8E%E5%8F%B2/%E5%8D%B7303)
* 『清史稿』
    * [卷一百六十六　公主表](https://zh.wikisource.org/wiki/%E6%B8%85%E5%8F%B2%E7%A8%BF/%E5%8D%B7166)
    * [卷二百一十四　后妃傳](https://zh.wikisource.org/wiki/%E6%B8%85%E5%8F%B2%E7%A8%BF/%E5%8D%B7214)
    * [巻五百〇八　列女傳一](https://zh.wikisource.org/wiki/%E6%B8%85%E5%8F%B2%E7%A8%BF/%E5%8D%B7508)
    * [巻五百〇九　列女傳二](https://zh.wikisource.org/wiki/%E6%B8%85%E5%8F%B2%E7%A8%BF/%E5%8D%B7509)
    * [巻五百一十　列女傳三](https://zh.wikisource.org/wiki/%E6%B8%85%E5%8F%B2%E7%A8%BF/%E5%8D%B7510)
    * [巻五百十一　列女傳四](https://zh.wikisource.org/wiki/%E6%B8%85%E5%8F%B2%E7%A8%BF/%E5%8D%B7511)

